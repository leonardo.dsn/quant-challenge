import indicators as i
import bot_abstract as sp


class Holdor(sp.Bot):

    def __init__(self, config=[0.975, 0.975, [24, 48, 72, 144], 15, 88, 14, 1.4, 14, 4]):

        stop_loss_short = config[0]
        stop_loss_long = config[1]
        tsm_periods = config[2]
        rsi_min = config[3]
        rsi_max = config[4]
        rsi_period = config[5]
        atr_value = config[6]
        atr_period = config[7]
        delay = config[8]

        self.tsm_fields = ['position_' + str(x) for x in tsm_periods]
        self.rsi_field = 'rsi_bool'+str(rsi_period)+'_'+str(rsi_min)+'_'+str(rsi_max)
        self.atr_field = 'atr'+str(atr_period)
        self.oper_field = 'oper'
        self.atr_value = atr_value
        self.stop_loss_short = stop_loss_short
        self.stop_loss_long = stop_loss_long
        self.delay = delay

    def decide(self, state, df):
        return self.evaluate(state, df)

    def evaluate(self, state, df):
        oper, atr, rsi, val = self.oper(df)
        action = self.choose_action(oper, state)

        result = {
            'ACTION': 'Hold',
            'QTY': 0,
            'MIN': 0,
            'MAX': 0,
            'BUY_NOW': False,
            'SELL_NOW': False,
            'CLOSE_NOW': False,
            'BOTLOG': {
                'atr': atr,
                'sum': val
            }
        }

        wallet = state['wallet']
        ask_price = state['ask_price']
        bid_price = state['bid_price']
        quantity = state['quantity']

        if action == "Long":
            result["ACTION"] = "Long"
            result["BUY_NOW"] = True
            result["CLOSE_NOW"] = quantity < 0
            result['QTY'] = int(wallet / ask_price)
            return result

        if action == "Short":
            result["ACTION"] = "Short"
            result["SELL_NOW"] = True
            result["CLOSE_NOW"] = quantity > 0
            result['QTY'] = int(wallet / bid_price)
            return result

        if action == "Close":
            result["ACTION"] = "Close"
            result["CLOSE_NOW"] = True
            return result

        return result

    def choose_action(self, operation, state):
        quantity = state['quantity']
        if operation > 0 > quantity:
            return 'Close'
        if operation < 0 < quantity:
            return 'Close'

        if operation > 0:
            if quantity <= 0:
                return 'Long'
            return "Hold"

        if operation < 0:
            if quantity >= 0:
                return 'Short'
            return "Hold"
        return "Hold"

    def oper(self, df):
        val = 0
        if df[self.atr_field].values[0] < self.atr_value:
            for field in self.tsm_fields:
                val += df[field].values[0]
            oper = 0 if abs(val) != len(self.tsm_fields) else -1 if val < 0 else 1
            oper += df[self.rsi_field].values[0]
            if df[self.oper_field].values[0] == 1 and oper == -1:
                oper = 0
            if df[self.oper_field].values[0] == -1 and oper == 1:
                oper = 0
            return oper, df[self.atr_field].values[0], df[self.rsi_field].values[0], val

        return 0, 0, 0, 0


def instantiate():
    return Holdor()
