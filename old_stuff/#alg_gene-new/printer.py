import json
import datetime

def line():
	return "*********************************************************************************\n"

def decision2json(decision):
	return "\n{\n" \
			"\t'ACTION': '"+str(decision["ACTION"])+"',\n" \
            "\t'SYMBOL': "+str(decision["SYMBOL"])+"',\n" \
            "\t'TIMESTAMP': "+str(int(datetime.datetime.now().timestamp()))+"',\n" \
			"\t'CLOSE_NOW': "+str(decision["CLOSE_NOW"])+"',\n" \
			"\t'BUY_NOW': "+str(decision["BUY_NOW"])+"',\n" \
			"\t'SELL_NOW': "+str(decision["SELL_NOW"])+"',\n" \
			"\t'QTY': "+str(decision["QTY"])+"',\n" \
		"}"

def json2str(value):
	return json.dumps(value, sort_keys=True, indent=4)