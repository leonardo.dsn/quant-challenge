# -*- coding: utf-8 -*-
import pandas as pd
import numpy as np
import config


def initialize(df):
    if 'var' in df.columns: return df
    df['var'] = df['close'] / df['close'].shift(1)  # maior que 1 indica que subiu
    df['returns'] = np.log(df['var'])
    df['change'] = df['close'] - df['close'].shift(1)
    return df

def tdm(df, tdm):
    df['count'] = 0.0
    df['nine'] = 0.0
    df['oper'] = 0.0
    for i in range(20, len(df)):
        df['count'].values[i] = 1 if df['close'].values[i] >= df['close'].values[i-4] else -1

        if df['count'].values[i] + df['count'].values[i-1] + df['count'].values[i-2] + df['count'].values[i-3] + df['count'].values[i-4] + df['count'].values[i-5] + df['count'].values[i-6] + df['count'].values[i-7] + df['count'].values[i-8]== 9: #or df['count'].values[i] + df['count'].values[i-1] + df['count'].values[i-2] + df['count'].values[i-3] + df['count'].values[i-4] + df['count'].values[i-5] + df['count'].values[i-6] + df['count'].values[i-7]==8:
            df['nine'].values[i] = 1
        if df['count'].values[i] + df['count'].values[i-1] + df['count'].values[i-2] + df['count'].values[i-3] + df['count'].values[i-4] + df['count'].values[i-5] + df['count'].values[i-6] + df['count'].values[i-7] + df['count'].values[i-8] == -9: #or df['count'].values[i] + df['count'].values[i-1] + df['count'].values[i-2] + df['count'].values[i-3] + df['count'].values[i-4] + df['count'].values[i-5] + df['count'].values[i-6] + df['count'].values[i-7]==-8:
            df['nine'].values[i] = -1

    for i in range(20, len(df)):
        if df['nine'].values[i] == -1:
            df['oper'].values[i] = 1
        if df['nine'].values[i] == 1:
            df['oper'].values[i] = -1
    return df

def tsm(df, periodos):
    for momentum in periodos:
        df['position_' + str(momentum)] = np.sign(df['returns'].rolling(momentum).mean())

    return df


def rsi(df, periodo, min, max):
    key = str(periodo)+'_'+str(min)+'_'+str(max)

    base = False
    if 'gain'+str(periodo) not in df.columns:
        base = True

        df['gain'+str(periodo)] = 0.0
        df['loss'+str(periodo)] = 0.0
        idx = df[df['change'] >= 0].index
        df.loc[idx, 'gain'+str(periodo)] = df.loc[idx, 'change']
        idx = df[df['change'] < 0].index
        df.loc[idx, 'loss'+str(periodo)] = df.loc[idx, 'change'] * (-1)
        df['gain_mean'+str(periodo)] = df['gain'+str(periodo)].rolling(periodo).mean()
        df['loss_mean'+str(periodo)] = df['loss'+str(periodo)].rolling(periodo).mean()
        df['rsi'+str(periodo)] = 0.0

    df['rsi_bool'+key] = 0
    rs = 0.0

    first_row = True
    for i in range(periodo, len(df)):

        if not first_row:
            if base:
                df['gain_mean'+str(periodo)].values[i] = ((df['gain_mean'+str(periodo)].values[i - 1] * (periodo - 1)) +
                                                          df['gain'+str(periodo)].values[i]) / periodo
                df['loss_mean'+str(periodo)].values[i] = ((df['loss_mean'+str(periodo)].values[i - 1] * (periodo - 1)) +
                                                          df['loss'+str(periodo)].values[i]) / periodo
                rs = df['gain_mean'+str(periodo)].values[i] / df['loss_mean'+str(periodo)].values[i]

        first_row = False

        if base:
            if not df['loss_mean'+str(periodo)].values[i] == 0:
                df['rsi'+str(periodo)].values[i] = 100 - (100 / (1 + rs))

        v = (df['rsi'+str(periodo)].values[i - 1] + df['rsi'+str(periodo)].values[i]) / 2

        if df['rsi'+str(periodo)].values[i] >= max and v > df['rsi'+str(periodo)].values[i - 2]:
            df['rsi_bool'+key].values[i] = -1
            continue

        if df['rsi'+str(periodo)].values[i] <= min and v < df['rsi'+str(periodo)].values[i - 2]:
            df['rsi_bool'+key].values[i] = 1
            continue

    return df


def atr(df, periodo):
    if 'atr' in df.columns: return df

    periodo = periodo
    base = False
    if 'tr' not in df.columns:
        base = True

    if base:
        df['atr_1'] = df['high'] - df['low']
        df['atr_2'] = abs(df['close'].shift(1) - df['high'])
        df['atr_3'] = abs(df['close'].shift(1) - df['low'])
        df['tr'] = df[['atr_1', 'atr_2', 'atr_3']].apply(np.max, axis=1)
        df['tr'] = df['tr'] * 100 / df['close'].shift(1)

    df['atr'+str(periodo)] = df['tr'].rolling(periodo).mean()

    return df


def pnl(state=None, bid_price=None, ask_price=None, entry_price=None, leverage=None):
    if state is not None:
        bid_price = state['bid_price']
        ask_price = state['ask_price']
        entry_price = state['entry_price']
        leverage = state['leverage']

    state = state if state is not None else {}
    state['short_pnl'] = 1
    state['long_pnl'] = 1

    if entry_price != 0.0:
        state['short_pnl'] = 1 + (-1 + entry_price / ask_price) * leverage
        state['long_pnl'] = 1 + (-1 + bid_price / entry_price) * leverage

    return state
