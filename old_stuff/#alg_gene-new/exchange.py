import bitmex
import datetime
import pandas
import json
import time
import bravado
import config
from indicators import pnl

RETRY_IN_SECONDS = 1

class Exchange:
	
	git = "master"
	prod = False
	
	def __init__(self, id_, prod_, symbol_):
		global broker, canceler, symbol
		symbol = symbol_
		self.prod = prod_
		broker = self.client(tp="broker", id=id_)
		canceler = self.client(tp="canceler", id=id_)
	
	
	def client(self, id, tp):
		
		data = config.access()
		self.git =  data['git']['prod' if self.prod else 'test']
		
		for entry in data['data']:
			candidate = entry['id']
			if not candidate==id: continue
			
			for key in entry['keys']:
				if not key['prod']==self.prod: continue
				if not key['type']==tp: continue
				
				return bitmex.bitmex(test=not self.prod, api_key=key['key'], api_secret=key['secret'])
		
		raise Exception("ERROR: None valid credential for '"+id+"'")


	def state(self):
		
		global _lastState
		
		#leverage = 1  # TODO: recuperar esse valor da API
		rst = canceler.Position.Position_get(filter=json.dumps({'symbol': symbol})).result()
		try:
			currentQty = rst[0][0]['currentQty']
			avgEntryPrice = rst[0][0]['avgEntryPrice']
		except:
			currentQty = 0
			avgEntryPrice = 0
			
		date = datetime.datetime.utcnow()
		date += datetime.timedelta(seconds=-30)
		quote = canceler.Quote.Quote_get(symbol=symbol, startTime=date).result()
		
		ask_price = quote[0][0]['askPrice']
		bid_price = quote[0][0]['bidPrice']
		
		_lastState = pnl({
			'prod': self.prod,
			'entry_price': avgEntryPrice,
			'leverage': 1,
			'quantity': currentQty,
			'wallet': canceler.User.User_getWallet().result()[0]['amount'],
			'ask_price': ask_price,
			'bid_price': bid_price,
			'symbol' : symbol
		})

		return _lastState
	
	
	def history(self, days=30):
		end = int(datetime.datetime.utcnow().timestamp())
		start = end-days*24*60*60
		url = "https://testnet.bitmex.com" if not self.prod else "https://www.bitmex.com"
		url = url + "/api/udf/history?symbol="+symbol+"&resolution=60&from=" +str(start) + "&to="+str(end)
		print(url)
		ds = pandas.read_json(url)
		del ds['s']
		ds.columns = ['close', 'hight', 'low', 'open', 'time', 'volumeto']
		return ds
	
	
	def execute(self, decision, state):
		
		symbol = state['symbol']
		
		if decision['CLOSE_NOW']:
			with open("./logs/current_coin.log", "w+") as fp:
				fp.write("")
			self.cancell_all(state, symbol)
		
		
		if decision['BUY_NOW']:
			with open("./logs/current_coin.log", "w+") as fp:
				fp.write(symbol)
			wallet = state['wallet']
			price = state['ask_price']
			quantity = abs(int(wallet*price/10**8))
			
			stoploss = abs(long_stoploss(state))
			self.new_order(symbol, quantity, -stoploss)
			
			fastwin = abs(long_fastwin(state))
			self.stop_order(symbol, quantity, fastwin)

			return
		
		
		if decision['SELL_NOW']:
			with open("./logs/current_coin.log", "w+") as fp:
				fp.write(symbol)
			wallet = state['wallet']
			price = state['bid_price']
			quantity = abs(int(wallet*price/10**8))
			
			stoploss = abs(short_stoploss(state))
			self.new_order(symbol, -quantity, stoploss)
			
			fastwin = abs(short_fastwin(state))
			self.stop_order(symbol, quantity, fastwin)
	
	
	def cancell_all(self, state, symbol):
		
		while True:
			if state['quantity']==0: return
			try:
				print(broker.Order.Order_new(symbol=symbol, orderQty=-state['quantity'], ordType='Market').result())
				break
			except bravado.exception.HTTPServiceUnavailable:
				print("Server Error 503...")
				time.sleep(RETRY_IN_SECONDS)
			except:
				pass
		
		while True:
			try:
				print(canceler.Order.Order_cancelAll().result())
				break
			except bravado.exception.HTTPServiceUnavailable:
				print("Server Error 503...")
				time.sleep(RETRY_IN_SECONDS)

	
	def new_order(self, symbol, quantity, stop):
		
		while True:
			try:
				print(broker.Order.Order_new(symbol=symbol, orderQty=-quantity, pegOffsetValue=stop, ordType='Stop', pegPriceType='TrailingStopPeg').result())
				break
			except bravado.exception.HTTPServiceUnavailable:
				print("Server Error 503...")
				time.sleep(RETRY_IN_SECONDS)
				
		while True:
			try:
				print(broker.Order.Order_new(symbol=symbol, orderQty=quantity).result())
				return
			except bravado.exception.HTTPServiceUnavailable:
				print("Server Error 503...")
				time.sleep(RETRY_IN_SECONDS)
	
	
	def stop_order(symbol, self, quantity, stop):
		#TODO escrever com o leo
		return

def long_stoploss(state):
	return int(state['ask_price']*(1-config.stoploss()['long']))


def short_stoploss(state):
	return int(state['bid_price']*(1-config.stoploss()['short']))


def long_fastwin(state):
	return int(state['ask_price']*config.fastwin()['long'])


def short_fastwin(state):
	return int(state['bid_price']*config.fastwin()['short'])