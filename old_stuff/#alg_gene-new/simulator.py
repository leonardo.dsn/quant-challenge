# -*- coding: utf-8 -*-
import pandas as pd

from indicators import pnl

import indicators as i


class Simulator:

    def __init__(self, investment, tax_, df, periods=30 * 24):

        self.investment = investment
        self.tax = tax_
        self.wallet = investment
        self.quantity = 0
        self.current_close = 0.0
        self.fees = 0.0
        self.stopPx = 0.0
        self.entry_price = 0.0
        self.progressive_stp = 0.0
        self.delay_activate = 0.0
        self.delay_TS = 0.0
        self.delay_sell = 0.0
        self.delay_buy = 0.0
        self.x = 0
        self.current_time = 0.0
        self.entry_price_action = None
        self.df = df[-periods:]

    def reset(self):
        self.wallet = self.investment
        self.quantity = 0
        self.current_close = 0.0
        self.fees = 0.0
        self.stopPx = 0.0
        self.entry_price = 0.0
        self.progressive_stp = 0.0
        self.delay_sell = 0
        self.delay_buy = 0
        self.delay_activate = 0
        self.delay_TS = 0
        self.x = 0
        self.entry_price_action = None

    def simulate(self, bot):
        self.reset()

        # todo verificar pq no anterior este valor equivale a 'self.df.shape[0] - 2'
        for i in range(0, self.df.shape[0] - 1):
            # todo verificar esta logica
            next_row = self.df[i + 1: i + 2]
            row = self.df[i: i + 1]

            state = self.state(next_row)
            state = pnl(state)

            decision = bot.decide(state, row)

            self.execute(state, decision, bot)

        state = self.state(row)
        state = pnl(state)

        decision['ACTION'] = 'Close'
        decision['CLOSE_NOW'] = True
        decision['BUY_NOW'] = False
        decision['SELL_NOW'] = False
        decision['BOTLOG'] = {}
        self.execute(state, decision, bot)

        return self.wallet

    def state(self, row):

        self.current_close = row['close'].values[0]

        result = {
            # 'timestamp': str(row['Date'].values[0])+','+str(row['Time'].values[0]),
            'timestamp': row['time'].values[0],
            'entry_price': self.entry_price / 1000,
            'leverage': 1,
            'quantity': self.quantity,
            'wallet': self.wallet,
            'ask_price': self.current_close / 1000,
            'bid_price': self.current_close / 1000
        }

        if self.quantity < 0 and self.progressive_stp > self.current_close:
            self.progressive_stp = self.current_close

        if self.quantity > 0 and self.progressive_stp < self.current_close:
            self.progressive_stp = self.current_close

        return result

    def execute(self, state, decision, bot):
        self.current_time = state['timestamp']
        if self.delay_activate == 1:
            self.delay_TS = state['timestamp']
            self.delay_activate = 0
        if self.delay_TS != 0:
            self.x =  self.current_time - self.delay_TS
        if self.x >= 3600*bot.delay:
            self.delay_TS = 0
            self.delay_sell = 0
            self.delay_buy = 0

        if not decision['CLOSE_NOW'] and not decision['SELL_NOW'] and not decision['BUY_NOW']:

            p = pnl(bid_price=self.current_close, ask_price=self.current_close,
                    entry_price=self.progressive_stp, leverage=state['leverage'])

            if state['quantity'] > 0 < p['long_pnl'] < bot.stop_loss_long:
                decision['ACTION'] = 'Close'
                decision['CLOSE_NOW'] = True
                decision['BOTLOG'] = {"break": "stop loss"}
                self.delay_buy = 1
                self.delay_activate = 1

            if state['quantity'] < 0 < p['short_pnl'] < bot.stop_loss_short:
                decision['ACTION'] = 'Close'
                decision['CLOSE_NOW'] = True
                decision['BOTLOG'] = {"break": "stop loss"}
                self.delay_sell = 1
                self.delay_activate = 1

        if decision['CLOSE_NOW']:
            self.stopPx = 0

            if self.quantity < 0:
                self.wallet += int(abs(self.quantity * self.entry_price ** 2 / self.current_close) / 10) / 100
                self.quantity = 0
                self.wallet = self.wallet * (1 - self.tax)
                if decision['BUY_NOW']:
                    decision['QTY'] = int(self.wallet * 1000 / self.current_close)
                self.delay_sell = 1
                self.delay_activate = 1

            if self.quantity > 0:
                self.wallet += int(abs(self.quantity * self.current_close) / 10) / 100
                self.quantity = 0
                self.wallet = self.wallet * (1 - self.tax)
                if decision['SELL_NOW']:
                    decision['QTY'] = -int(self.wallet * 1000 / self.current_close)
                self.delay_buy = 1
                self.delay_activate = 1

            self.entry_price = 0
            self.entry_price_action = None
            self.progressive_stp = 0

        if decision['BUY_NOW'] and self.delay_buy != 1:
            qty = abs(int(decision['QTY'] * 0.999))
            self.fees = abs(int(abs(qty * self.tax * self.current_close / 10)) / 100)
            qty_us = abs(int(qty * self.current_close / 10) / 100)
            qty = abs(int((qty_us - self.fees) * 1000 / self.current_close))
            self.quantity += qty
            self.wallet -= int(qty * self.current_close / 10 + self.fees * 100) / 100
            self.stopPx = self.long_stoploss(bot, state)

            if not self.entry_price_action == 'buy':
                self.entry_price = self.current_close
                self.entry_price_action = 'buy'
                self.progressive_stp = self.entry_price

            return

        if decision['SELL_NOW'] and self.delay_sell != 1:
            qty = abs(int(decision['QTY'] * 0.999))
            self.fees = abs(int(abs(qty * self.tax * self.current_close / 10)) / 100)
            qty_us = abs(int(qty * self.current_close / 10) / 100)
            qty = abs(int((qty_us - self.fees) * 1000 / self.current_close))
            self.quantity -= qty
            self.wallet -= int(qty * self.current_close / 10 + self.fees * 100) / 100
            self.stopPx = self.short_stoploss(bot, state)

            if not self.entry_price_action == 'sell':
                self.entry_price = self.current_close
                self.entry_price_action = 'sell'
                self.progressive_stp = self.entry_price

    @staticmethod
    def long_stoploss(bot, state):
        return int(state['ask_price']*(1-bot.stop_loss_long))

    @staticmethod
    def short_stoploss(bot, state):
        return int(state['bid_price']*(1-bot.stop_loss_short))