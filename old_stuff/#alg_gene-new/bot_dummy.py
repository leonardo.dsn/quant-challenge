# -*- coding: utf-8 -*-
import bot_abstract as sp
import random

class Dummy(sp.Bot):
	
	def decide(self, states, dfs, symbols,cur_symbol):
		
		result = {
			'QTY': 0,
			'MIN': 0,
			'MAX': 0,
			'BUY_NOW': False,
			'SELL_NOW': False,
			'CLOSE_NOW': False,
			'BOTLOG': {}
		}
	
		# SET ACTION:
		action = 0
		#action = random.randint(0, 3)
		
		quantity = states[0]['quantity']
		ask_price = states[0]['ask_price']
		wallet = states[0]['wallet']
	
		if action==0:
			result["SYMBOL"] = symbols[0]
			result["ACTION"] = "Hold"
			result["CLOSE_NOW"] = False
			return states[0], result
		
		if action==1:
			result["SYMBOL"] = symbols[0]
			result["ACTION"] = "Long"
			result["CLOSE_NOW"] = quantity<0
			result["BUY_NOW"] = True
			result['QTY'] = int(wallet/ask_price)
			return states[0], result
		
		if action==2:
			result["SYMBOL"] = symbols[0]
			result["ACTION"] = "Short"
			result["CLOSE_NOW"] = quantity>0
			result["SELL_NOW"] = True
			result['QTY'] = -int(wallet/ask_price)
			return states[0], result
		
		if action==3:
			result["SYMBOL"] = symbols[0]
			result["ACTION"] = "Close"
			result["CLOSE_NOW"] = True
			return states[0], result
		
		return states[0], result


def instantiate():
	return Dummy()
