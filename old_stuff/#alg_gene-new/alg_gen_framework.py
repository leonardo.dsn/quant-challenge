import random as rd
import operator
import pickle

class Gene(object):

    def sample(self):
        raise NotImplemented

    def cross(self, other, mutation_rate):
        if rd.random() < mutation_rate:
            return self.sample()

        return rd.choice([self, other])

    def info(self):
        return ''


class Chromosome(object):

    def __init__(self, genes=[]):
        self.genes = genes

    @staticmethod
    def sample():
        raise NotImplemented

    def info(self):
        str_info = ''
        for gene in self.genes:
            str_info += 'Gen[{}]\t'.format(gene.info())
        return str_info

    @classmethod
    def cross(cls, father, mother, mutation_rate):
        assert(len(father.genes) == len(mother.genes))

        new_genes = []

        for gen1, gen2 in zip(father.genes, mother.genes):
            new_genes.append(gen1.cross(gen2, mutation_rate))

        new_dna = cls()
        new_dna.genes = new_genes

        return new_dna


class Population(object):

    def __init__(self, generations, population_size, winners_to_keep, target_fitness = None,
                 mutation_rate=0.1, subjects=None, save_file = None):
        self.population_size = population_size
        self.subjects = []
        self.winners_to_keep = winners_to_keep
        self.winners = []
        self.mutation_rate = mutation_rate
        self.generations = generations
        self.target_fitness = target_fitness
        self.save_file = save_file

        if subjects is not None:
            for i in range(len(subjects)):
                self.winners.append([0.0, subjects[i]])

    def evaluate(self, subject):
        raise NotImplemented

    def generate_subjects(self):
        self.subjects = []
        for element in self.winners:
            self.subjects.append(element)

        to_gen = self.population_size-len(self.subjects)

        for i in range(to_gen):
            father = rd.choice(self.winners)[1]
            mother = father

            if len(self.winners) < 2:
                self.winners.append(father.sample())

            while father == mother:
                mother = rd.choice(self.winners)[1]

            self.subjects.append([0, father.cross(father, mother, self.mutation_rate)])

    def fitness(self):
        s = len(self.subjects)
        for i in range(s):
            self.subjects[i][0] = self.evaluate(self.subjects[i][1])
            pct = int((i+1)/s * 100)
            # print('\rOK', end='')
            print('\r[{:03d}%]'.format(pct), end='', flush=True)
        self.winners = sorted(self.subjects, key=operator.itemgetter(0), reverse=False)[-self.winners_to_keep:]
        print('')
        # for element in reversed(self.winners):
        #     print('fitness:', element[0])

    def evolve(self):
        for g in range(1, self.generations+1):
            print('generation -', g)
            self.generate_subjects()
            self.fitness()
            print('-'*15)
            for i, winner in enumerate(reversed(self.winners)):
                print('ranking:{} | fitness:{} | info:{}'.format(i+1, winner[0], winner[1].info()))
            print('-'*45)

            if self.save_file is not None:
                print('saving to file', self.save_file)
                with open(self.save_file, 'wb') as fp:
                    pickle.dump(self.winners, fp)

            if self.target_fitness is not None and self.winners[-1][0] >= self.target_fitness:
                break

        print('-' * 45)
        print('evolution done in {} generations'.format(g))
        print('WINNER | fitness:{} | info:{}'.format(self.winners[-1][0], self.winners[-1][1].info()))




