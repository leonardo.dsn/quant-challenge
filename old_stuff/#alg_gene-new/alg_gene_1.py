# -*- coding: utf-8 -*-
import pandas as pd
import random as rd
import numpy as np
import string
string.ascii_letters 
'ABCDEFGHIJKLMNOPQRSTUVWXYZ'

fitness = ['p','a','r','a','l','e','l','e','p','i','p','e','d','o','w']
gene = []

def gene_teste(gene):
    teste = 0
    for i in range(0,13):
        if gene [i] == fitness[i]:
           teste += 1
    return teste


def gerar_ind(gene,fator_mut):
    gene_pai = gene
    del gene[14]
    df = pd.DataFrame()
    for idx in range(0,13):
        gene[idx] = gene_pai[idx]
        if rd.randint(0, 10) > fator_mut:
            gene[idx] = rd.choice(string.ascii_letters)
   
    gene.append(gene_teste(gene))
    return gene

def melhor_individuo(gene,populacao):
    geracao = pd.DataFrame()
    for i in range(1,populacao):
        individuo = gerar_ind(gene,7)
        
        geracao = geracao.append([individuo], ignore_index=True)
    geracao = geracao.sort_values(by = [14],ascending = False)
    geracao = geracao.reset_index()
    geracao = geracao.drop(14,axis = 1)
    melhor = geracao.iloc[0,]
    return melhor


def evolucionario(gene,geracoes):
    melhor = pd.DataFrame()
    for i in range(1,geracoes):
        melhor = melhor_individuo(gene,1000)
        melhor = melhor.tolist()
        o_cara = gerar_ind(melhor,7)
    return melhor
       
print(evolucionario(['a','a','a','a','a','a','a','a','a','a','a','a','a','a',0],50))
