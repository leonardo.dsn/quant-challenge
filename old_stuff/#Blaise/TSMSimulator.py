# -*- coding: utf-8 -*-
import matplotlib as plt
import numpy as np

sb=0.96
ss=1.04
mx=1.45

def __buy(trade, strat, cash_coins_lvrg, tax, lvrg):
    
    time = strat['time'].values[0]
    x = strat['close'].values[0] 
    
    if lvrg < 1: lvrg=1  
    cash=cash_coins_lvrg[0]
    cash_coins_lvrg[0] = 0
    cash_coins_lvrg[1] = (cash*(1-tax))/x  
    cash_coins_lvrg[2] = cash_coins_lvrg[1] * (lvrg-1)   
    cash_coins_lvrg[3] = x 
    cash_coins_lvrg[4] = time
    cash_coins_lvrg[5] = 0

    p_time = '%.2f' % time
    p_price = '%.2f' % x
    p_cash = '%.2f' % cash
    printing = 'LONG     '+'Time: '+ p_time + '  Price: ' + p_price + '   Cash: ' + p_cash
    print(printing)

    return trade+1


def __sell(trade, strat, cash_coins_lvrg, tax, lvrg, sim = False):
    time = strat['time'].values[0]
    x =strat['close'].values[0] 
    
    lvrgTax = 2 - 0.9999375
    tmp=0
    if lvrg < 1: lvrg=1  
    if lvrg>1:
        dTimeH = (time - cash_coins_lvrg[4])/3600
        tmp = - cash_coins_lvrg[2]*cash_coins_lvrg[3] # - preco inicial
        taxHrs = (lvrgTax ** dTimeH)
        tmp = tmp * taxHrs # - taxa por hora
        tmp = tmp + cash_coins_lvrg[2]*x # + preco final
        if not sim:
            cash_coins_lvrg[5] = tmp

    coins=cash_coins_lvrg[1]
    val = x*coins*(1-tax) + tmp
    if sim:
        return val/(cash_coins_lvrg[1]*cash_coins_lvrg[3]+cash_coins_lvrg[2]*cash_coins_lvrg[3])

    cash_coins_lvrg[0] = val
    cash_coins_lvrg[1] = 0
    cash_coins_lvrg[2] = 0

    p_time = '%.2f' % time
    p_price = '%.2f' % x
    p_cash = '%.2f' % val
    printing = 'CLOSE    '+'Time: '+ p_time + '  Price: ' + p_price + '   Cash: ' + p_cash
    print(printing)

    return trade+1

def __stop_sell(trade, strat, cash_coins_lvrg, tax, lvrg, sim = False):
    time = strat['time'].values[0]
    x =cash_coins_lvrg[3]*sb
    
    lvrgTax = 2 - 0.9999375
    tmp=0
    if lvrg < 1: lvrg=1  
    if lvrg>1:
        dTimeH = (time - cash_coins_lvrg[4])/3600
        tmp = - cash_coins_lvrg[2]*cash_coins_lvrg[3] # - preco inicial
        taxHrs = (lvrgTax ** dTimeH)
        tmp = tmp * taxHrs # - taxa por hora
        tmp = tmp + cash_coins_lvrg[2]*x # + preco final
        if not sim:
            cash_coins_lvrg[5] = tmp

    coins=cash_coins_lvrg[1]
    val = x*coins*(1-tax) + tmp
    if sim:
        return val/(cash_coins_lvrg[1]*cash_coins_lvrg[3]+cash_coins_lvrg[2]*cash_coins_lvrg[3])

    cash_coins_lvrg[0] = val
    cash_coins_lvrg[1] = 0
    cash_coins_lvrg[2] = 0

    cash_coins_lvrg[0] = val
    cash_coins_lvrg[1] = 0
    cash_coins_lvrg[2] = 0

    p_time = '%.2f' % time
    p_price = '%.2f' % x
    p_cash = '%.2f' % val
    printing = 'CLOSE    '+'Time: '+ p_time + '  Price: ' + p_price + '   Cash: ' + p_cash + '          STOPLOSS!'
    print(printing)


    return trade+1

def __short(trade, strat, cash_coins_lvrg, tax, lvrg):
    
    time = strat['time'].values[0]
    x = strat['close'].values[0] 
    
    if lvrg < 1: lvrg=1  
    
    cash=cash_coins_lvrg[0]
    cash_coins_lvrg[1] = -(cash*(1-tax))/x  
    cash_coins_lvrg[2] = cash_coins_lvrg[1] * (lvrg-1)   
    cash_coins_lvrg[3] = x 
    cash_coins_lvrg[4] = time
    cash_coins_lvrg[5] = 0

    p_time = '%.2f' % time
    p_price = '%.2f' % x
    p_cash = '%.2f' % cash
    printing = 'SHORT    '+'Time: '+ p_time + '  Price: ' + p_price + '   Cash: ' + p_cash
    print(printing)


    return trade+1


def __close_short(strat, cash_coins_lvrg, tax, lvrg, sim = False):
    time = strat['time'].values[0]
    x =strat['close'].values[0] 
    
    lvrgTax = 2 - 0.9999375
    tmp=0
    if lvrg < 1: lvrg=1  
    
    dTimeH = (time - cash_coins_lvrg[4])/3600
    taxHrs = (lvrgTax ** dTimeH)
    
    
    if lvrg>1:
#        tmp = cash_coins_lvrg[2]*cash_coins_lvrg[3] # - preco inicial
        tmp = cash_coins_lvrg[2]*x # - preco inicial
        tmp = tmp * taxHrs * (1+tax)# - taxa por hora
        tmp = tmp + (-cash_coins_lvrg[2]*cash_coins_lvrg[3]) # + preco final
        ptmp = tmp/(-cash_coins_lvrg[2]*cash_coins_lvrg[3])
        if not sim:
            cash_coins_lvrg[5] = tmp

    coins=cash_coins_lvrg[1]
    
    val = (-coins*cash_coins_lvrg[3]) - (-coins*x*(1+tax)*taxHrs) + tmp
    
    if sim:
        return (cash_coins_lvrg[0]+val)/((np.abs(coins*cash_coins_lvrg[3]))+np.abs(cash_coins_lvrg[2]*cash_coins_lvrg[3]))
        
    cash_coins_lvrg[0] += val
    cash_coins_lvrg[1] = 0
    cash_coins_lvrg[2] = 0

    p_time = '%.2f' % time
    p_price = '%.2f' % x
    p_cash = '%.2f' % cash_coins_lvrg[0]
    printing = 'CLOSE    '+'Time: '+ p_time + '  Price: ' + p_price + '   Cash: ' + p_cash
    print(printing)
    
def __stop_short(strat, cash_coins_lvrg, tax, lvrg, sim = False):
    time = strat['time'].values[0]
    x =cash_coins_lvrg[3]*ss
    
    lvrgTax = 2 - 0.9999375
    tmp=0
    if lvrg < 1: lvrg=1  
    
    dTimeH = (time - cash_coins_lvrg[4])/3600
    taxHrs = (lvrgTax ** dTimeH)
    
    
    if lvrg>1:
#        tmp = cash_coins_lvrg[2]*cash_coins_lvrg[3] # - preco inicial
        tmp = cash_coins_lvrg[2]*x # - preco inicial
        tmp = tmp * taxHrs * (1+tax)# - taxa por hora
        tmp = tmp + (-cash_coins_lvrg[2]*cash_coins_lvrg[3]) # + preco final
        ptmp = tmp/(-cash_coins_lvrg[2]*cash_coins_lvrg[3])
        if not sim:
            cash_coins_lvrg[5] = tmp

    coins=cash_coins_lvrg[1]
    
    val = (-coins*cash_coins_lvrg[3]) - (-coins*x*(1+tax)*taxHrs) + tmp
    
    if sim:
        return (cash_coins_lvrg[0]+val)/((np.abs(coins*cash_coins_lvrg[3]))+np.abs(cash_coins_lvrg[2]*cash_coins_lvrg[3]))
        
    cash_coins_lvrg[0] += val
    cash_coins_lvrg[1] = 0
    cash_coins_lvrg[2] = 0

    p_time = '%.2f' % time
    p_price = '%.2f' % x
    p_cash = '%.2f' % cash_coins_lvrg[0]
    printing = 'CLOSE    '+'Time: '+ p_time + '  Price: ' + p_price + '   Cash: ' + p_cash
    print(printing)

def avg_strat(strat, fields):

    val = 0
    if strat['atr'].values[0] < mx:
        for field in fields:
            val+=strat[field].values[0]
            ind=0 if abs(val) != len(fields) else -1 if val < 0 else 1
        ind+=strat['rsi_bool'].values[0]
        return ind
    else:
        return 0

# def avg_strat(strat, fields):
#     val = 0
#     if strat['atr'].values[0] < mx:
#         ind = strat['oper'].values[0]
#
#         return ind
#     else:
#         return 0

def simulate(investment, base, positions, strats, tax, useShort, lvrg, useLong = True, longShort = True):
    
    df = base.dropna()
    cash_coins_lvrg = [investment, 0, 0, 0, 0, 0]
    trade = 0
    
    log = []
    log_coins = []
    log_lvrg = []
    log_tmp = []

    
#    field = positions[0][1]
    fields = [x[1] for x in positions]

    stopLossS = False
    stopLossB = False
    
    for idx in range(len(df)):
        strat = df[idx:idx+1]
        
        
#        value = stop(cash_coins_lvrg, strat)
        
        oper = avg_strat(strat, fields)
        
        PnL = 0
        
        if cash_coins_lvrg[1] < 0:
            PnL = __close_short(strat, cash_coins_lvrg, tax, lvrg, sim=True)
        if cash_coins_lvrg[1] > 0:
            PnL = __sell(trade, strat, cash_coins_lvrg, tax, lvrg, sim=True)
        
        try:
            if (useShort and oper>0 and cash_coins_lvrg[1] < 0) or ((PnL<0.993 or PnL>1.021) and PnL > 0 and useShort and cash_coins_lvrg[1] < 0):
                __close_short(strat, cash_coins_lvrg, tax, lvrg)
                stopLossS = True
                if not useLong:
                    stopLossS = False
                if not useLong or not longShort:
                    trade+=1
                    continue
            
            if  (cash_coins_lvrg[3]*ss <= strat['high'].values[0] and PnL > 0 and useShort and cash_coins_lvrg[1] < 0):
                __stop_short(strat, cash_coins_lvrg, tax, lvrg)
                stopLossS = True
                if not useLong:
                    stopLossS = False
                if not useLong or not longShort:
                    trade+=1
                    continue
                
            if useLong and oper>0 and cash_coins_lvrg[1] == 0 and not stopLossB:
                trade = __buy(trade, strat, cash_coins_lvrg, tax, lvrg)
                stopLossS = False
                continue
                
            if (useLong and oper<0 and cash_coins_lvrg[1] > 0) or ((PnL < 0.993 or PnL>1.021) and PnL > 0 and useLong and cash_coins_lvrg[1] > 0):
                trade = __sell(trade, strat, cash_coins_lvrg, tax, lvrg)
                stopLossB = True
                if not useShort:
                    stopLossB = False
                if not useShort or not longShort:
                    continue
                trade -= 1 #ajustar para o short
                
            if  (cash_coins_lvrg[3]*sb >= strat['low'].values[0] and PnL > 0 and useLong and cash_coins_lvrg[1] > 0):
                trade = __stop_sell(trade, strat, cash_coins_lvrg, tax, lvrg)
                stopLossB = True
                if not useShort:
                    stopLossB = False
                if not useShort or not longShort:
                    continue
                trade -= 1 #ajustar para o short
          
            if useShort and oper<0 and cash_coins_lvrg[1] == 0 and not stopLossS:
                trade = __short(trade, strat, cash_coins_lvrg, tax, lvrg)
                stopLossB = False
#                cash_coins_lvrg[0] = cash_coins_lvrg[0]*(2- strat['variacao'].values[0])
                
            continue
        finally:
            log.append(cash_coins_lvrg[0])
            log_coins.append(cash_coins_lvrg[1])
            log_lvrg.append(cash_coins_lvrg[2])
            log_tmp.append(cash_coins_lvrg[5])
        
        log.append(0)
        
    if cash_coins_lvrg[1] > 0:
       log.append(__sell(trade, strat, cash_coins_lvrg, tax, lvrg))
    
    fluxo = [0]
    for l in log:
        fluxo.append(fluxo[-1]+l)
        
#    base[strats].dropna().cumsum().apply(np.exp).plot()
#    plt.pyplot.figure(figsize=(15,10))
#    plt.pyplot.plot(fluxo)
#     plt.pyplot.figure(figsize=(15,10))
#     plt.pyplot.plot(log_coins)
#     plt.pyplot.figure(figsize=(15,10))
#     plt.pyplot.plot(log)
#    plt.pyplot.figure(figsize=(15,10))
#    plt.pyplot.plot(log_tmp)
     
    max = len(df)
    strat0 = df[0:1]
    strat1 = df[max-1:max]
    pos1 = investment/strat0["open"].values[0]
    total = pos1*strat1["close"].values[0]

    print("")
    print("Total da investimento: %s" %int(investment))
    print("Total da inercia: %s" %int(total))
    print("Total de dinheiro: %s" %int(cash_coins_lvrg[0]))
    
    dif = int(cash_coins_lvrg[0]-total)
    print("Diferenca: %s" %dif)
    print("Resultado: %s" %int(dif/investment))  
    
    print("Total de moedas: %s" %int(cash_coins_lvrg[1]))
    print("Total de trades: %s \n \n" %trade)