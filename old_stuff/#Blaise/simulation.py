# -*- coding: utf-8 -*-
import pandas as pd
import numpy as np
import scipy as sci
import TSMSimulator as si


def readDatabase(fileName):
    base = pd.read_csv(fileName, sep=',')
    base['variacao'] = base['close'] / base['close'].shift(1)  # maior que 1 indica que subiu
    base['returns'] = np.log(base['variacao'])
    base['change'] = base['close'] - base['close'].shift(1)
    return base

def calcPositions(periodos, base):
    positions = []
    for momentum in periodos:
        data = []
        data.append(momentum)
        data.append('position_' + str(momentum))
        base[data[1]] = np.sign(base['returns'].rolling(momentum).mean())
        positions.append(data)
    return positions


def calcStrats(positions):
    strats = ['returns']
    for p in positions:
        strat = 'Strategy_%s' % p[0]
        base[strat] = base[p[1]].shift(1) * ((base['returns']))
        strats.append(strat)
    return strats


def calcRSI(periodo, max, min):
    for linha in range(len(base)):
        if base['change'].values[linha] > 0:
            base.loc[linha, 'gain'] = base['change'].values[linha]
            continue
        if base['change'].values[linha] < 0:
            base.loc[linha, 'loss'] = base['change'].values[linha] * (-1)
            continue
        
    base.fillna(0, inplace=True)
    base['gain_mean'] = base['gain'].rolling(periodo).mean()
    base['loss_mean'] = base['loss'].rolling(periodo).mean()

    for i in range(periodo + 1, len(base)):
        base.loc[i, 'gain_mean'] = ((base['gain_mean'].values[i - 1] * (periodo - 1)) + base['gain'].values[
            i]) / periodo
        base.loc[i, 'loss_mean'] = ((base['loss_mean'].values[i - 1] * (periodo - 1)) + base['loss'].values[
            i]) / periodo
    base['rs'] = base['gain_mean'] / base['loss_mean']

    for a in range(periodo, len(base)):
        if base['loss_mean'].values[a] == 0:
            base['rsi'] = 0
            continue
        base.loc[a, 'rsi'] = 100 - (100 / (1 + base['rs'].values[a]))

    for j in range(periodo, len(base)):
        if base['rsi'].values[j] >= max:
            base.loc[j, 'rsi_bool'] = -1
            continue
        if base['rsi'].values[j] <= min:
            base.loc[j, 'rsi_bool'] = 1
            continue
        base.loc[j, 'rsi_bool'] = 0

    return base
    
def calcATR(base,periodo):
    periodo = periodo
    base['atr_1'] = base['high']-base['low']
    base['atr_2'] = abs(base['close'].shift(1)-base['high'])
    base['atr_3'] = abs(base['close'].shift(1)-base['low'])
    base['tr'] = base[['atr_1','atr_2','atr_3']].apply(np.max,axis=1)
    base['tr'] = base['tr']*100/base['close'].shift(1)
    base['atr'] = base['tr'].rolling(periodo).mean()
    return base

def max_value(inputlist):
    return max([sublist[-1] for sublist in inputlist])

def simTriangle(base, timeframe):
    base['triangle'] = 0
    for i in range (timeframe, len(base)-timeframe):
        highs = []
        lows = []
        okay = 0
        for x in range (0, timeframe):
            highs.append([base['high'].values[i-x], base['time'].values[i-x]])
            lows.append([base['low'].values[i-x], base['time'].values[i-x]])
        lows.sort()
        highs.sort(reverse=True)

        for j in range(0,int(timeframe/6)):
            if highs[j][0] <1:
                continue
            if abs(highs[j][1] - highs[j+1][1]) <= 3600*2:
                highs[j+1] = highs[j+2]
                highs[j+2] = highs[j+3]
                highs[j + 3] = highs[j + 4]
                highs[j + 4] = highs[j + 5]
                highs[j + 5] = highs[j+6]
                highs[j+6] = [0,0]
                highs[j + 7] = [0, 0]
                highs[j + 8] = [0, 0]
                highs[j + 9] = [0, 0]
                highs[j + 10] = [0, 0]
                highs[j + 11] = [0, 0]

        if highs[0][1] < highs[1][1] < highs[2][1] < highs[3][1]:
            if base['triangle'].values[i-1] + base['triangle'].values[i-2] + base['triangle'].values[i-3] + base['triangle'].values[i-4] + base['triangle'].values[i-5] == 0:
                okay =1

        for j in range(0,int(timeframe/6)):
            if lows[j][0] <1:
                continue
            if abs(lows[j][1] - lows[j+1][1]) <= 3600*2:
                lows[j+1] = highs[j+2]
                lows[j+2] = highs[j+3]
                lows[j + 3] = highs[j + 4]
                lows[j + 4] = highs[j + 5]
                lows[j + 5] = highs[j+6]
                lows[j+6] = [0,0]
                lows[j + 7] = [0, 0]
                lows[j + 8] = [0, 0]
                lows[j + 9] = [0, 0]
                lows[j + 10] = [0, 0]
                lows[j + 11] = [0, 0]

        if lows[0][1] < lows[1][1] < lows[2][1] < lows[3][1]:
            if base['triangle'].values[i-1] + base['triangle'].values[i-2] + base['triangle'].values[i-3] + base['triangle'].values[i-4] + base['triangle'].values[i-5] == 0 and okay ==1:
                base['triangle'].values[i] = 5
                print(base['time'].values[i])
                j = '%.2f' % timeframe
                print('SYMMETRICAL TRIANGLE   ' + j)
                projection = highs[0][0]-lows[0][0]
                base['projection'] = projection
                continue
        base['triangle'].values[i] = 0
    return base

def ascTriangle(base, timeframe):
    base['triangle'] = 0
    for i in range (timeframe, len(base)-timeframe):
        highs = []
        lows = []
        okay = 0
        for x in range (0, timeframe):
            highs.append([base['high'].values[i-x], base['time'].values[i-x]])
            lows.append([base['low'].values[i-x], base['time'].values[i-x]])
        lows.sort()
        highs.sort(reverse=True)

        for j in range(0,int(timeframe/6)):
            if highs[j][0] <1:
                continue
            if abs(highs[j][1] - highs[j+1][1]) <= 3600*2:
                highs[j+1] = highs[j+2]
                highs[j+2] = highs[j+3]
                highs[j + 3] = highs[j + 4]
                highs[j + 4] = highs[j + 5]
                highs[j + 5] = highs[j+6]
                highs[j+6] = [0,0]
                highs[j + 7] = [0, 0]
                highs[j + 8] = [0, 0]
                highs[j + 9] = [0, 0]
                highs[j + 10] = [0, 0]
                highs[j + 11] = [0, 0]

        if highs[0][0] * 0.989 < highs[3][0] < highs[2][0] < highs[1][0]:
            if base['triangle'].values[i-1] + base['triangle'].values[i-2] + base['triangle'].values[i-3] + base['triangle'].values[i-4] + base['triangle'].values[i-5] == 0:
                okay =1


        for j in range(0,int(timeframe/6)):
            if lows[j][0] <1:
                continue
            if abs(lows[j][1] - lows[j+1][1]) <= 3600*2:
                lows[j+1] = highs[j+2]
                lows[j+2] = highs[j+3]
                lows[j + 3] = highs[j + 4]
                lows[j + 4] = highs[j + 5]
                lows[j + 5] = highs[j+6]
                lows[j+6] = [0,0]
                lows[j + 7] = [0, 0]
                lows[j + 8] = [0, 0]
                lows[j + 9] = [0, 0]
                lows[j + 10] = [0, 0]
                lows[j + 11] = [0, 0]

        if lows[0][1] < lows[1][1] < lows[2][1] < lows[3][1]:
            if base['triangle'].values[i-1] + base['triangle'].values[i-2] + base['triangle'].values[i-3] + base['triangle'].values[i-4] + base['triangle'].values[i-5] == 0 and okay ==1:
                base['triangle'].values[i] = 1
                print(base['time'].values[i])
                j = '%.2f' % timeframe
                print('ASCENDING TRIANGLE    ' + j)
                projection = highs[0][0]-lows[0][0]
                base['projection'] = projection
                continue
        base['triangle'].values[i] = 0
    return base


def descTriangle(base, timeframe):
    base['triangle'] = 0
    for i in range(timeframe, len(base) - timeframe):
        highs = []
        lows = []
        okay = 0
        for x in range(0, timeframe):
            highs.append([base['high'].values[i - x], base['time'].values[i - x]])
            lows.append([base['low'].values[i - x], base['time'].values[i - x]])
        lows.sort()
        highs.sort(reverse=True)

        for j in range(0, int(timeframe / 6)):
            if highs[j][0] < 1:
                continue
            if abs(highs[j][1] - highs[j + 1][1]) <= 3600 * 2:
                highs[j + 1] = highs[j + 2]
                highs[j + 2] = highs[j + 3]
                highs[j + 3] = highs[j + 4]
                highs[j + 4] = highs[j + 5]
                highs[j + 5] = highs[j + 6]
                highs[j + 6] = [0, 0]
                highs[j + 7] = [0, 0]
                highs[j + 8] = [0, 0]
                highs[j + 9] = [0, 0]
                highs[j + 10] = [0, 0]
                highs[j + 11] = [0, 0]

        if highs[0][1] < highs[1][1] < highs[2][1] < highs[3][1]:
            okay = 1

        for j in range(0, int(timeframe / 6)):
            if lows[j][0] < 1:
                continue
            if abs(lows[j][1] - lows[j + 1][1]) <= 3600 * 2:
                lows[j + 1] = highs[j + 2]
                lows[j + 2] = highs[j + 3]
                lows[j + 3] = highs[j + 4]
                lows[j + 4] = highs[j + 5]
                lows[j + 5] = highs[j + 6]
                lows[j + 6] = [0, 0]
                lows[j + 7] = [0, 0]
                lows[j + 8] = [0, 0]
                lows[j + 9] = [0, 0]
                lows[j + 10] = [0, 0]
                lows[j + 11] = [0, 0]

        if lows[0][0] * 1.011 > lows[3][0] > lows[2][0] > lows[1][0]:
            if base['triangle'].values[i - 1] + base['triangle'].values[i - 2] + base['triangle'].values[i - 3] + base['triangle'].values[i - 4] + base['triangle'].values[i - 5] == 0 and okay != 0:
                base['triangle'].values[i] = -1
                print(base['time'].values[i])
                j = '%.2f' % timeframe
                print('DESCEDING TRIANGLE    ' + j)
                projection = highs[0][0]-lows[0][0]
                base['projection'] = projection
                continue
        base['triangle'].values[i] = 0
    return base

def oper(base):

    return base


'BTC'
base = readDatabase('./BTC_Bitstamp.csv')
#base = readDatabase('../Research/bitmex_5min_1month.csv')
#base = readDatabase('../Crawler/old_data/BTC_2year_hist.csv')
# base = readDatabase('C:/Users/LPC/Documents/TradeHack/Python/DADOS/Convertidos/convertido/BTC_Last_360.csv')
#base = readDatabase('C:/Users/LPC/Documents/TradeHack/Python/DADOS/Convertidos/convertido/BTC_Last_2000min.csv')
#base = readDatabase('../Broker/logs/history.csv')

'Traditional-Markets'
# base = readDatabase('C:/Users/LPC/Documents/TradeHack/Python/DADOS/Convertidos/convertido/USD_JPY.csv')

'Tabela'
base = base[500:10877]

'Janelas-Main'
base = base[:]
base = base.reset_index(drop = True)
base = calcRSI(12, 88, 15)
base = calcATR(base,12)

base = simTriangle(base, 72)
base = simTriangle(base, 40)
base = simTriangle(base, 30)
base = simTriangle(base, 20)
base = simTriangle(base, 15)


base = ascTriangle(base, 15)
base = ascTriangle(base, 30)

base = descTriangle(base, 15)
base = descTriangle(base, 30)

#positions = calcPositions([4,6,8,10,12,14,24,48], base) # - Short_Term

positions = calcPositions([24,48,72,144], base)  # - Long_Term

strats = calcStrats(positions)

si.simulate(100000, base, positions, strats, 0.00075, useShort=True, lvrg=1, useLong=True, longShort=False)

# print(positions)