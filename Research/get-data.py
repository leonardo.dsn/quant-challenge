from quantopian.pipeline import Pipeline
from quantopian.pipeline.data.factset import Fundamentals
from quantopian.pipeline.domain import US_EQUITIES
from quantopian.research import run_pipeline

# Construct a factor that simply gets the most recent quarterly sales value.
quarterly_sales = Fundamentals.sales_qf.latest

# Add the factor to the pipeline.
pipe = Pipeline(
    columns={
        'sales': quarterly_sales,
    },
    domain=US_EQUITIES,
)

# Run the pipeline over a year and print the result.
df = run_pipeline(pipe, '2015-05-05', '2016-05-05')
print(df.head())
