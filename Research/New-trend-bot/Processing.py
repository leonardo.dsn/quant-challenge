import Simulator as si
import os
import GetData
import datetime
from multiprocessing import Process
import pandas as pd

begin_time = datetime.datetime.now()

def oneSim(initDate, size_carteira, meses, CPU, fileName, simulacao, dados_loc, simulation_params, useLong = True, useShort = False, ativos = []):
    atr_max = simulation_params[0]
    usar_SMAL11 = simulation_params[1]
    pop_repeat = simulation_params[2]
    atr_period = simulation_params[3]
    S = si.Simulation(initDate, meses, ativos, size_carteira, dados_loc, useLong, useShort, CPU, fileName, usar_SMAL11, simulacao, atr_max, pop_repeat, atr_period, 22 * 6)
    return S

def run(size_carteira, months, initDate, fileName, threads, simulacao, dados_loc, simulation_params):
    global processes
    processes = []
    # CPUs = os.cpu_count() - 8
    CPUs = threads
    daysList = GetData.list(months, initDate)
    init_count_per_cpu = int(months/CPUs)
    count_per_cpu = []
    if init_count_per_cpu == 0:
        init_count_per_cpu = 1
        CPUs = months
        for i in range(months):
            count_per_cpu.append(1)
    else:
        for i in range(CPUs):
            count_per_cpu.append(init_count_per_cpu)

        i = 0
        rest = months % CPUs
        while rest > 0:
            count_per_cpu[i] += 1
            i += 1
            rest -= 1

    for i in range(CPUs):
        count_per_cpu[i] -= 1


    '''IMPLEMENTAR SEPARACAO DOS MESES POR CORE DA CPU  '''
    current = 0
    for CPU in range(CPUs): #11 CPUS, 0-10
        # print('registering processes %d' % CPU)
        #initDate, size_carteira, meses

        for i in range(0,CPU): #NA CPU 0, NAO RODA O FOR!
            current +=  count_per_cpu[i]+1
        initDate = daysList[current]
        month_int = count_per_cpu[CPU]

        processes.append(Process(target=oneSim, args=(initDate, size_carteira, month_int, CPU, fileName, simulacao, dados_loc, simulation_params)))
        current = 0

    for process in processes:
        process.start()

    for process in processes:
        process.join()
        # process.terminate()

    return ''


def createLog(initDate, months, size_carteira, logLocation = './logs/'):
    for i in range(10000):
        try:
            open(logLocation + 'result_log-' + str(i) + '.csv')
        except:
            F = open(logLocation + 'result_log-' + str(i) + '.csv', "w+")
            # F.write('size = ' + str(size_carteira)+'; months = ' + str(months) + '; initDate = ' + initDate + '\n')
            F.write('result,data,num_ativos,lista_ativos,oper_ativos,volatilidade')
            F.close()
            break

    return logLocation + 'result_log-' + str(i) + '.csv'

def runner(initDate,months,size_carteira,dados_loc, threads, simulacao, simulation_params):
    if simulacao:
        fileName = createLog(initDate, months, size_carteira)
    else:
        fileName = 'NAN'
        dados_loc = '../strategyData/'
    run(size_carteira,months,initDate, fileName, threads, simulacao, dados_loc, simulation_params)

    '''END CODE - READ TXT AND MAKE CALCULATIONS'''

    if simulacao:
        base = pd.read_csv(fileName, sep=',')
        acumulado = 1
        for i in range(len(base)):
            acumulado = acumulado * base['result'].values[i]

        print(acumulado)
        print('size = ' + str(size_carteira)+'; months = ' + str(months) + '; initDate = ' + initDate)
        print(datetime.datetime.now() - begin_time)

    for process in processes:
        process.terminate()

''' ------------- RUN MAIN CODE AND WRITE FILE ----------------'''

dados_loc = '../histData/'
size_carteira = 9 #8 IS BEST
atr_max = 1.1
# months = 120+7+4
months = 120
usar_SMAL11 = False
threads = 8
simulacao = True
pop_repeat = False #TRUE
atr_period = 14 #22 BEST
# initDate = 'Feb 28, 2016'
initDate = 'Dec 31, 2009'
# initDate = 'Aug 31, 2020'

# simulation_params = [atr_max,usar_SMAL11,pop_repeat,atr_period]
# runner(initDate,months,size_carteira,dados_loc, threads, simulacao, simulation_params)

sizes =  [7,8,9,10,12,15]
atrs = [0.95, 1, 1.05, 1.1, 1.15, 1.2, 1.3]

for size_carteira in sizes:
    for atr_max in atrs:
        print(' ------- ATR = '+str(atr_max) + ', SIZE = ' + str(size_carteira) + ' -------')
        simulation_params = [atr_max, usar_SMAL11, pop_repeat, atr_period]
        runner(initDate,months,size_carteira,dados_loc, threads, simulacao, simulation_params)

'''DEFAULTS DO ROBO
dados_loc = '../histData/'
size_carteira = 9
atr_max = 1.1
# months = 120+7+4
months = 120
usar_SMAL11 = False
threads = 4
simulacao = True
pop_repeat = False
atr_period = 14
# initDate = 'Feb 28, 2016'
initDate = 'Dec 31, 2009'
# initDate = 'Aug 31, 2020'
'''